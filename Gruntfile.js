module.exports = function (grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    cssmin: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        livereload: true,
        mergeIntoShorthands: false,
        roundingPrecision: -1,
        sourceMap: {
          includeSources: true
        },
      },
      target: {
        files: {
          'public/css/bundle.css': ['resources/assets/css/*.css', '!*.min.css']
        }
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        livereload: true,
        sourceMap: {
          includeSources: true
        },
      },
      build: {
        src: ['resources/assets/js/*.js', '!*.min.js'],
        dest: 'public/js/bundle.min.js'
      }
    },
    htmlmin: {
      dist: {
        options: {
          banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
          removeComments: false,
          collapseWhitespace: true
        },
        files: {
          'public/index.html': 'public/index.html'
        }
      }
    },
    filehash: {
      options: {
        output: 'static/hash.json'
      },
      js: {
        cwd: 'public/',
        src: 'js/**/*.js',
        dest: 'public/'
      },
      css: {
        cwd: 'public/',
        src: 'css/**/*.css',
        dest: 'public/'
      },
    },
    clean: ['public*//*.min.*']
  });
  grunt.loadNpmTasks('grunt-banner');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-file-hash');
  grunt.registerTask('default', ['cssmin', 'uglify', 'htmlmin', 'clean', 'filehash']);
  grunt.registerTask('build', ['cssmin', 'uglify', 'htmlmin', 'filehash']);
};